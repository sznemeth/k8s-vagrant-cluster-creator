# Main config properties
$minion_count = 1 # Calico needs at least 1 minion to be added
$token = '034e19.6fc2feea9efd7589' # kubeadm token generate can be used to generate a random one => moved into configure.sh
$master_memory = 1536
$minion_memory = 1536
$master_ip = '10.10.10.10'
$api_server_port = '6443'
$deploy_calico = 'true' # Installing Calico as pod network provider (optional)
$deploy_heapster = 'false' # Installing Heapster / Grafana for monitoring (optional)
