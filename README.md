# k8s dummy cluster creator

Dummy kubernetes cluster for learning, testing purposes. By default Vagrant creates a 2 node cluster (1 x master, 1 x minion), installs kubernetes, docker etc. and creates a cluster on the master node with the `kubeadm` untility.

## Prerequisites

- Virtualbox (<https://www.virtualbox.org/>) as virtualization platform
- Vagrant (<https://www.vagrantup.com/>) to manage the virtual nodes

## VM and service properties

These config options can be found in the properties.rb file. The default settings are the following.

```
# Main config properties
$minion_count = 1 # Calico needs at least 1 minion to be added
$token = '034e19.6fc2feea9efd7589' # kubeadm token generate can be used to generate a random one => moved into configure.sh
$master_memory = 1536
$minion_memory = 1536
$master_ip = '10.10.10.10'
$api_server_port = '6443'
$deploy_calico = 'true' # Installing Calico as pod network provider (optional)
$deploy_heapster = 'false' # Installing Heapster / Grafana for monitoring (optional)
```

I use Calico as pod network manager and Heapster / Garfana for monitoring, these components are optional and can be toggled off or on from the above config file. If you wish to install your own pod network manager just toggle Calico off, cluster will come up just fine with out it. Heapster (since it's useless without pod network it only work if both Calico and Heapster is toggled on) deployment is optional but if you do deploy it look for the web URL in the install logs.

## Usage

1. Clone the git repository
2. Change the directory to the project's directory
3. Create hosts

  `vagrant up`

  **I use `nfs` shares instead of synced folders so during the creation of the host your user password will be asked!**

4. When the hosts are up use the kubectl utility to connect to the cluster (if you have a Mac the tool van be installed, otherwise use the one on the `master` host as root)

  `kubectl --kubeconfig=./kubeconfig get pods --all-namespaces`

5. Destroy the cluster when you are done.

  `vagrant destroy`

## Tasks

- config shell script is a mess, has to find a better, clearer way to do this
- Vargantfile's code need some re-org here and there
- `kubectl --kubeconfig` is a pain to use, alias support has to be added
