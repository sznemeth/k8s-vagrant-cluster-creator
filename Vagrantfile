# -*- mode: ruby -*-
# vi: set ft=ruby :

load 'properties.rb'

VAGRANTFILE_API_VERSION = "2"
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'

#  Randomly generated IP address for the minions
def generate_minion_ip(count)
  return "10.10.10.#{count+110}"
end

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ GENERAL CONFIG ]

  config.vm.box = "centos/7" # Choice of operating systemctl
  config.vm.synced_folder ".", "/nfs", type: "nfs" # Shared storage for VMs

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ MASTER ]

  config.vm.define "master" do |master|
    master.vm.network "private_network", ip: $master_ip
    master.vm.hostname = "master"
    master.vm.provider "virtualbox" do |v|
        v.memory = $master_memory
    end
    master.vm.provision "shell" do |s|
      s.inline = "sh /vagrant/configure.sh $1 $2 $3 $4 $5"
      s.args = %W( #{$master_ip} #{$token} #{$api_server_port} #{$deploy_calico} #{$deploy_heapster} )
    end
  end

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ MINIONS ]

  ( 1..$minion_count ).each do |i|
    config.vm.define "minion#{i}" do |minion|
      minion.vm.network "private_network", ip: "#{generate_minion_ip(i)}"
      minion.vm.hostname = "minion#{i}"
      minion.vm.provider "virtualbox" do |v|
        v.memory = $minion_memory
      end
      minion.vm.provision "shell" do |s|
        s.inline = "sh /vagrant/configure.sh $1 $2 $3"
        s.args = %W( #{$master_ip} #{$token} #{$api_server_port} )
      end
    end
  end
end
