#!/bin/bash

cd /nfs/calico && rm -f calicoctl*

GITHUB_URL='https://github.com/'
GITHUB_API_URL='https://api.github.com/repos'
GITHUB_ENDPOINT='releases'
GITHUB_POWNER='projectcalico'
GITHUB_PNAME='calicoctl'
GITHUB_RELEASE='latest'

VERSION=$(curl -s "${GITHUB_API_URL}/${GITHUB_POWNER}/${GITHUB_PNAME}/${GITHUB_ENDPOINT}/${GITHUB_RELEASE}" | grep 'tag_name' | awk -F'[:"]' '{ print $5 }')

wget -q "${GITHUB_URL}/${GITHUB_POWNER}/${GITHUB_PNAME}/${GITHUB_ENDPOINT}/download/${VERSION}/calicoctl"
