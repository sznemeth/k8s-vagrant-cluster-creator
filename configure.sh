#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -[ ENTRIES TO HOSTS FILE ]

echo "10.10.10.10 master controller" >> /etc/hosts

for host in {1..9}
do
  echo "10.10.10.11${host} minion${host} worker${host}" >> /etc/hosts
done

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ TURNING OFF THE SHIELDS ]

setenforce 0

echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.conf
sysctl -p &>/dev/null

# Disabling iptables & firewalld
systemctl stop iptables 2>/dev/null && systemctl disable iptables 2>/dev/null
systemctl stop firewalld && systemctl disable firewalld

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ CREATING YUM REPOS ]

yum install -y epel-release

# Create and enable kubernetes yum repository
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

yum update -y

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ INSTALLING COMPONENTS ]
yum install -y ntp docker kubelet kubeadm kubernetes-cni wget figlet git net-tools

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ ENABLING SERVICES ]
systemctl enable ntpd && systemctl start ntpd
systemctl enable docker && systemctl start docker
systemctl enable kubelet && systemctl start kubelet

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ LET THE SHOW BEGIN! ]

if [ $HOSTNAME == 'master' ]
then
    figlet -w 170 "I am your $HOSTNAME now! IP: `hostname -I | awk '{ print $2 }'`"
    kubeadm init --apiserver-advertise-address=$1 --token=$2 --apiserver-bind-port=$3 --token-ttl 0 --pod-network-cidr=192.168.0.0/16
    cp /etc/kubernetes/admin.conf /nfs/kubeconfig

    # Installing Calico as pod network provider (optional)
    if [ $4 == 'true' ]
    then
      echo "Installing Calico ... "
      sh /nfs/calico/get_calicoctl_latest.sh
      kubectl --kubeconfig=/nfs/kubeconfig apply -f /nfs/calico/calico.yaml
      cp /nfs/calico/calicoctl /usr/sbin/ && chmod +x /usr/sbin/calicoctl
      echo "export ETCD_ENDPOINTS=http://10.96.232.136:6666" >> ~/.bash_profile
    fi

    # Installing Heapster as monitoring tool (optional)
    if [ $4 == 'true' ] && [ $5 == 'true' ]
    then
      echo "Installing Heapster ... "
      kubectl --kubeconfig=/nfs/kubeconfig create -f /nfs/heapster/deployment/
      kubectl --kubeconfig=/nfs/kubeconfig create -f /nfs/heapster/heapster-rbac.yaml
      grafana_ext_port=$(kubectl --kubeconfig=/nfs/kubeconfig get services monitoring-grafana --namespace=kube-system -o json | grep nodePort | awk -F'[":,]' '{ print $4 }' |  tr -d " ")
      echo "Grafana URL: http://$1:${grafana_ext_port}/"
    fi

else
    figlet -w 170 "$HOSTNAME is reporting for duty! IP: `hostname -I | awk '{ print $2 }'`"
    kubeadm join $1:$3 --token=$2
    cp /nfs/calicoctl /usr/sbin/ && chmod +x /usr/sbin/calicoctl
fi
